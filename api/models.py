from django.db import models
from django.contrib.auth.models import AbstractUser


# class User was extended to support authorization
class User(AbstractUser):
    class Status(models.IntegerChoices):
        INACTIVE = 0
        ACTIVE = 1
    status = models.IntegerField(choices=Status.choices, default=Status.ACTIVE)


class Board(models.Model):
    topic = models.TextField()
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="boards")
    participants = models.ManyToManyField(User)


class Todo(models.Model):
    class Status(models.IntegerChoices):
        TODO = 1
        DONE = 2
        BLOCKED = 3

    INCOMPLETE_STATUSES = [Status.TODO, Status.BLOCKED]

    title = models.CharField(max_length=100)
    description = models.TextField()
    due_date = models.DateField()
    status = models.IntegerField(choices=Status.choices, default=Status.TODO)
    todo_list = models.ForeignKey(Board, on_delete=models.CASCADE)
