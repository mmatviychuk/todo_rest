from rest_framework import status
from rest_framework.response import Response

from api.models import Board


def object_exist_or_404(model):
    def inner_function(func):
        def wrapper(*args, **kwargs):
            pk = kwargs.get('pk')
            if not model.objects.filter(id=pk).exists():
                return Response(status=status.HTTP_404_NOT_FOUND)
            return func(*args, **kwargs)
        return wrapper
    return inner_function


def is_owner(func):
    def wrapper(*args, **kwargs):
        pk = kwargs.get('pk')
        request = args[1]
        board = Board.objects.get(id=pk)
        if not board.owner_id == request.user.id:
            return Response(status=status.HTTP_403_FORBIDDEN)
        return func(*args, **kwargs)
    return wrapper
