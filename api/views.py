from rest_framework import status
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from api.decorators import (
    object_exist_or_404,
    is_owner,
)

from api.models import (
    Board,
    User,
    Todo,
)
from api.serializers import (
    ListBoardSerializer,
    DetailedBoardSerializer,
    TodoSerializer,
)


class ListBoardsView(APIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        boards = Board.objects.all()
        serializer = ListBoardSerializer(boards, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        owner_id = request.data.get('owner_id')
        participant_ids = request.data.get('participant_ids', [])
        if not User.objects.filter(id=owner_id).exists():
            return Response(data={'owner_id': ['incorrect value']}, status=status.HTTP_400_BAD_REQUEST)

        users = User.objects.filter(id__in=participant_ids)
        users_ids = [usr.id for usr in users]
        for participant_id in participant_ids:
            if participant_id not in users_ids:
                return Response(data={'participant_ids': ['incorrect value']}, status=status.HTTP_400_BAD_REQUEST)

        serializer = DetailedBoardSerializer(data=request.data)

        if serializer.is_valid():
            board = serializer.save(owner_id=owner_id)
            for usr in users:
                board.participants.add(usr)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class BoardView(APIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]

    @object_exist_or_404(Board)
    def get(self, request, pk, format=None):
        board = Board.objects.get(id=pk)
        serializer = DetailedBoardSerializer(board)
        return Response(serializer.data)

    @object_exist_or_404(Board)
    def put(self, request, pk, format=None):
        if 'topic' not in request.data:
            return Response(data={'topic': ['required field']}, status=status.HTTP_400_BAD_REQUEST)

        board = Board.objects.get(id=pk)
        serializer = DetailedBoardSerializer(board, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @object_exist_or_404(Board)
    @is_owner
    def delete(self, request, pk, format=None):
        board = Board.objects.get(id=pk)
        board.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class BoardTodoListView(APIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]

    @object_exist_or_404(Board)
    def get(self, request, pk, format=None):
        incomplete = request.GET.get('incomplete', 'False').lower() == 'true'
        todos = Todo.objects.filter(todo_list_id=pk)
        if incomplete:
            todos = todos.filter(status__in=Todo.INCOMPLETE_STATUSES)
        serializer = TodoSerializer(todos, many=True)
        return Response(serializer.data)

    @object_exist_or_404(Board)
    def post(self, request, pk, format=None):
        serializer = TodoSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save(todo_list_id=pk)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class BoardTodoView(APIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]

    @object_exist_or_404(Board)
    def put(self, request, pk, todo_pk, format=None):
        todo = Todo.objects.filter(id=todo_pk, todo_list_id=pk).first()
        if not todo:
            return Response(status=status.HTTP_404_NOT_FOUND)
        serializer = TodoSerializer(todo, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @object_exist_or_404(Board)
    def delete(self, request, pk, todo_pk, format=None):
        todo = Todo.objects.filter(id=todo_pk, todo_list_id=pk).first()
        if not todo:
            return Response(status=status.HTTP_404_NOT_FOUND)
        if not todo.todo_list.participants.filter(id=request.user.id).exists():
            return Response(status=status.HTTP_403_FORBIDDEN)
        todo.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
