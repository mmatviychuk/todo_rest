from django.urls import path

from . import views

urlpatterns = [
    path('boards', views.ListBoardsView.as_view(), name='board_list'),
    path('boards/<int:pk>', views.BoardView.as_view(), name='board_item'),
    path('boards/<int:pk>/todos', views.BoardTodoListView.as_view(), name='todo_list'),
    path('boards/<int:pk>/todos/<int:todo_pk>', views.BoardTodoView.as_view(), name='todo_item'),
]
