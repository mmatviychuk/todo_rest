from rest_framework import serializers

from api.models import User, Board, Todo


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'status']


class TodoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Todo
        fields = ['id', 'title', 'description', 'due_date', 'status']


class BasicBoardSerializer(serializers.ModelSerializer):
    owner = UserSerializer(required=False, read_only=True)
    participants = UserSerializer(many=True, required=False, read_only=True)

    class Meta:
        model = Board
        fields = ['id', 'topic', 'owner', 'participants']


class ListBoardSerializer(BasicBoardSerializer):
    todo_count = serializers.SerializerMethodField(method_name='get_todo_count')

    def get_todo_count(self, obj):
        return obj.todo_set.count()

    class Meta:
        model = Board
        fields = BasicBoardSerializer.Meta.fields + ['todo_count']


class DetailedBoardSerializer(BasicBoardSerializer):
    todos = serializers.SerializerMethodField(method_name='get_todos')

    class Meta:
        model = Board
        fields = BasicBoardSerializer.Meta.fields + ['todos']

    def get_todos(self, obj):
        return TodoSerializer(obj.todo_set.all(), many=True).data
