import datetime
import json

from django.test import TestCase

from django.urls import reverse
from .models import Board, User, Todo


class AuthBasicView(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.credentials = {
            'username': 'superuser',
            'password': '12345'
        }
        cls.user = User.objects.create_user(**cls.credentials)
        cls.user.first_name = 'John'
        cls.user.last_name = 'Smith'
        cls.user.save()

    @classmethod
    def tearDownClass(cls):
        cls.user.delete()
        super().setUpClass()


class ListBoardsViewTests(AuthBasicView):
    def setUp(self) -> None:
        super(ListBoardsViewTests, self).setUp()
        self.board = Board.objects.create(
            owner_id=self.user.id,
            topic='grocery'
        )
        self.board.participants.add(self.user)

    def test_list_boards(self):
        self.client.login(**self.credentials)
        response = self.client.get(reverse('board_list'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]['topic'], self.board.topic)
        self.assertEqual(response.data[0]['owner']['first_name'], self.user.first_name)
        self.assertEqual(response.data[0]['owner']['last_name'], self.user.last_name)
        self.assertEqual(len(response.data[0]['participants']), 1)
        self.assertEqual(response.data[0]['todo_count'], 0)

    def test_create_board_error(self):
        self.client.login(**self.credentials)
        data = {
            "topic": "house",
            "owner_id": self.user.id,
            "participant_ids": [1, 2, 3]
        }
        response = self.client.post(reverse('board_list'), data=json.dumps(data), content_type="application/json")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, {'participant_ids': ['incorrect value']})

    def test_create_board_success(self):
        self.client.login(**self.credentials)
        user2 = User.objects.create(username='simple_user', first_name='James', last_name='Smith')
        data = {
            "topic": "house",
            "owner_id": self.user.id,
            "participant_ids": [user2.id]
        }
        response = self.client.post(reverse('board_list'), data=json.dumps(data), content_type="application/json")
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data['topic'], data["topic"])
        self.assertEqual(response.data['owner']['first_name'], self.user.first_name)
        self.assertEqual(response.data['owner']['last_name'], self.user.last_name)
        self.assertEqual(len(response.data['participants']), 1)
        self.assertEqual(response.data['participants'][0]['id'], user2.id)


class BoardTodoListViewTests(AuthBasicView):
    def setUp(self) -> None:
        super(BoardTodoListViewTests, self).setUp()
        self.board = Board.objects.create(
            owner_id=self.user.id,
            topic='grocery'
        )
        self.board.participants.add(self.user)
        self.todo1 = Todo.objects.create(
            title='title1',
            description='Lorem ipsum',
            due_date=datetime.date.today(),
            status=Todo.Status.TODO,
            todo_list_id=self.board.id
        )
        self.todo2 = Todo.objects.create(
            title='title2',
            description='Lorem ipsum',
            due_date=datetime.date.today(),
            status=Todo.Status.DONE,
            todo_list_id=self.board.id
        )

    def test_list_todos(self):
        self.client.login(**self.credentials)
        response = self.client.get(reverse('todo_list', kwargs={'pk': self.board.id}))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data[0]['id'], self.todo1.id)
        self.assertEqual(response.data[0]['title'], self.todo1.title)
        self.assertEqual(response.data[0]['description'], self.todo1.description)
        self.assertEqual(response.data[0]['status'], self.todo1.status)
        self.assertEqual(response.data[1]['id'], self.todo2.id)
        self.assertEqual(response.data[1]['title'], self.todo2.title)
        self.assertEqual(response.data[1]['description'], self.todo2.description)
        self.assertEqual(response.data[1]['status'], self.todo2.status)

    def test_list_todos_incomplete(self):
        self.client.login(**self.credentials)
        url = reverse('todo_list', kwargs={'pk': self.board.id})
        response = self.client.get(f'{url}?incomplete=true')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]['id'], self.todo1.id)
        self.assertEqual(response.data[0]['title'], self.todo1.title)
        self.assertEqual(response.data[0]['description'], self.todo1.description)
        self.assertEqual(response.data[0]['status'], self.todo1.status)
