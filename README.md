###### Setup

`git clone https://mmatviychuk@bitbucket.org/mmatviychuk/todo_rest.git`

`cd todo_rest`

Create virtualenv

```virtualenv venv -p python3.8```


Install the dependencies

`pip install -r requirements.txt`

Run migrations:

`python ./manage.py migrate`

Run the project:

`python ./manage.py runserver`

To create user use command:

`python ./manage.py createuser`

User details could be edited via admin panel. 
`http://127.0.0.1:8000/admin/`



###### Database
Dump of the db with few items is added - file **dump.sql**

All users from the dump have password "12345". To make user logged in use admin panel login. 

`http://127.0.0.1:8000/admin/`



###### Access the api

List of the boards:

http://127.0.0.1:8000/api/boards

Board details:

http://127.0.0.1:8000/api/boards/ID

Todos list:

http://127.0.0.1:8000/api/boards/ID/todos

http://127.0.0.1:8000/api/boards/ID/todos?incomplete=true

Todos actions:

http://127.0.0.1:8000/api/boards/ID/todos/TODO_ID




###### Tests

`python ./manage.py test`
